﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private GamePanel gamePanel;
    [SerializeField]
    private TitlePanel titlePanel;
    [SerializeField]
    private WaitingPanel waitingPanel;
    [SerializeField]
    private BallSelectionPanel ballSelectionPanel;
    [SerializeField]
    private GameEndPanel gameEndPanel;
    private const string SEARCHING_MATCH_TEXT = "Searching match...";
    private const string WAITING_OPPONENT_TEXT = "Waiting opponent...";

    private void Awake() {
        ServiceProvider.Instance.Game.onWin += OnGameEnd;
        titlePanel.singlePlayerButton.onClick.AddListener(OnSinglePlayerButton);
        titlePanel.createMatchButton.onClick.AddListener(OnCreateMatchButton);
        titlePanel.joinMatchButton.onClick.AddListener(OnJoinMatchButton);
        waitingPanel.cancelButton.onClick.AddListener(OnCancelMatchButton);
    }

    private void OnSinglePlayerButton() {
        titlePanel.Hide();
        ballSelectionPanel.Show((ballPrefab) =>
        {
            ServiceProvider.Instance.GameInitializer.InitLocalGame(ballPrefab, null);
            ballSelectionPanel.Hide();
            gamePanel.Show();
            OnStartGame();
        });
    }

    private void OnJoinMatchButton() {
        titlePanel.Hide();
        waitingPanel.Show(SEARCHING_MATCH_TEXT);
        ServiceProvider.Instance.GameInitializer.InitNetworkGame(null, () =>
          {
              waitingPanel.Hide();
              gamePanel.Show();
              OnStartGame();
          });
    }

    private void OnCreateMatchButton() {
        titlePanel.Hide();
        ballSelectionPanel.Show((ballPrefab) =>
        {
            ballSelectionPanel.Hide();
            waitingPanel.Show(WAITING_OPPONENT_TEXT);
            ServiceProvider.Instance.GameInitializer.InitNetworkGame(ballPrefab, () =>
            {
                waitingPanel.Hide();
                gamePanel.Show();
                OnStartGame();
            });
        });
    }

    private void OnCancelMatchButton() {
        titlePanel.Show();
        waitingPanel.Hide();
        ServiceProvider.Instance.GameInitializer.DisposeGame();
    }

    private void OnGameEnd(IPlayer winner) {
        gameEndPanel.Show(winner.Name);
    }

    public void OnStartGame() {
        ServiceProvider.Instance.Game.playerOne.onScoreChanged += gamePanel.playerOneScoreUI.SetScore;
        ServiceProvider.Instance.Game.playerTwo.onScoreChanged += gamePanel.playerTwoScoreUI.SetScore;
        gamePanel.playerOneScoreUI.SetScore(0);
        gamePanel.playerTwoScoreUI.SetScore(0);
    }

    public void StopGame() {
        gameEndPanel.Hide();
        gamePanel.Hide();
        titlePanel.Show();
        ServiceProvider.Instance.Game.playerOne.onScoreChanged -= gamePanel.playerOneScoreUI.SetScore;
        ServiceProvider.Instance.Game.playerTwo.onScoreChanged -= gamePanel.playerTwoScoreUI.SetScore;
        ServiceProvider.Instance.GameInitializer.DisposeGame();
    }
}
