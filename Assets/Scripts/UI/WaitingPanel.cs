﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitingPanel : UIPanel
{
    public Button cancelButton;
    [SerializeField]
    private Text waitingLabel;

    public void Show(string text) {
        base.Show();
        SetText(text);
    }

    public void SetText(string text) {
        waitingLabel.text = text;
    }
}
