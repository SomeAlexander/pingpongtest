﻿using UnityEngine;
using UnityEngine.UI;

public class UIConsole : MonoSingletone<UIConsole>
{
    [SerializeField]
    private Text text;
    [SerializeField]
    private GameObject root;
    public Image scrollMask;

    private void LogMessage(string message) {
        text.text = message + text.text;
    }

    public static void Log(string message) {
        message = System.DateTime.Now.ToString("hh:mm:ss:fff ") + message + "\n";
        Debug.Log(message);
        Instance.LogMessage(message);
    }

    public static void LogFormat(string format, params object[] args) {
        Log(string.Format(format, args));
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.BackQuote)) {
            SwapVisibility();
        } else if(Input.GetKeyDown(KeyCode.C)) {
            Clear();
        }
    }

    public void Clear() {
        text.text = string.Empty;
    }

    public void SwapVisibility() {
        root.SetActive(!root.activeSelf);
    }
}