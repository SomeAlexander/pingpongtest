﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System;

public class CustomNetworkManager : NetworkManager
{
    public NetworkPlayer LocalPlayer { get; private set; }
    public NetworkPlayer RemotePlayer { get; private set; }
    public event Action<IPlayer> onPlayerAdded;
    public event Action onPlayersReady;
    public event Action onDisconnect;

    public void AddPlayer(NetworkPlayer player) {
        if(player.isLocalPlayer) {
            LocalPlayer = player;
        } else {
            RemotePlayer = player;
        }
        if(LocalPlayer != null && RemotePlayer != null) {
            onPlayersReady?.Invoke();
        }
    }

    public void CreateMatch() {
        if(matchMaker == null) {
            StartMatchMaker();
        }
        matchMaker.CreateMatch(matchName, matchSize, true, "", "", "", 0, 0, base.OnMatchCreate);
    }

    public void TryJoinMatch() {
        if(matchMaker == null) {
            StartMatchMaker();
        }
        matchMaker.ListMatches(0, 10, "", false, 0, 0, (searchSuccess, searchExtendedInfo, matchList) =>
        {
            base.OnMatchList(searchSuccess, searchExtendedInfo, matchList);
            if(searchSuccess && matches != null && matches.Count > 0) {
                //Conntect to first founded match
                MatchInfoSnapshot randomMatch = matches[0];
                matchName = randomMatch.name;
                matchMaker.JoinMatch(randomMatch.networkId, "", "", "", 0, 0, (joinSuccess, joinExtendedInfo, responseData) =>
               {
                   base.OnMatchJoined(joinSuccess, joinExtendedInfo, responseData);
               });
            }
        });
    }

    public void StopMatch() {
        StopMatchMaker();
        if(NetworkServer.active && IsClientConnected()) {
            StopHost();
        }
    }

    public override void OnClientDisconnect(NetworkConnection conn) {
        base.OnClientDisconnect(conn);
        onDisconnect?.Invoke();
    }

    public override void OnServerDisconnect(NetworkConnection conn) {
        base.OnServerDisconnect(conn);
        onDisconnect?.Invoke();
    }

    public override void OnDropConnection(bool success, string extendedInfo) {
        base.OnDropConnection(success, extendedInfo);
        onDisconnect?.Invoke();
    }
}
