﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LocalGameInitializer : AbstractGameInitializer
{
    public LocalPlayer localPlayer_1;
    public LocalPlayer localPlayer_2;

    public override void Init(BallData ball, Action onSuccess) {
        ServiceProvider.Instance.Game.playBoard.bottomPlatform.player = localPlayer_1;
        ServiceProvider.Instance.Game.playBoard.upperPlatform.player = localPlayer_2;
        ServiceProvider.Instance.Game.StartNewGame(localPlayer_1, localPlayer_2, Instantiate(ball.prefab));
        onSuccess?.Invoke();
    }

    public override void Dispose() {
        ServiceProvider.Instance.Game.EndGame();
    }
}
