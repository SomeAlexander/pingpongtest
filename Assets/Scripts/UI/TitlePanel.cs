﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TitlePanel : UIPanel {
    public Button singlePlayerButton;
    public Button joinMatchButton;
    public Button createMatchButton;
}
