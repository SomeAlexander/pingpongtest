﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BallSelectionPanel : UIPanel
{
    [SerializeField]
    private BallSelectionButton ballButtonPrefab;
    [SerializeField]
    private Transform buttonsContainer;
    public event Action<BallData> onBallSelected;

    private void Awake() {
        Init(ServiceProvider.Instance.Balls.balls);
    }

    public void Init(BallData[] ballsData) {
        foreach(BallData ballData in ballsData) {
            BallSelectionButton b = Instantiate(ballButtonPrefab);
            b.Init(ballData);
            b.transform.SetParent(buttonsContainer, false);
            b.onClick.AddListener(() =>
            {
                SelectBall(ballData);
            });
        }
    }

    private void SelectBall(BallData ballData) {
        onBallSelected?.Invoke(ballData);
    }

    public void Show(Action<BallData> selectCallback) {
        base.Show();
        onBallSelected = selectCallback;
    }

    public override void Hide() {
        base.Hide();
        onBallSelected = null;
    }
}
