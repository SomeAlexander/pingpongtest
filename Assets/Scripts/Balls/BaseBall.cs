﻿using UnityEngine;

public class BaseBall : MonoBehaviour
{
    public float speed;
    [SerializeField]
    private Rigidbody2D rb;

    public void StartMove(Vector2 direction) {
        rb.velocity = direction.normalized * speed;
    }

    public void SetPosition(Vector2 position) {
        rb.MovePosition(position);
    }

    private void Stop() {
        rb.velocity = Vector2.zero;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if(collision.contacts.Length > 0) {
            StartMove(Vector2.Reflect(rb.velocity, collision.contacts[0].normal));
        }
    }

    public virtual void Reset() {
        Stop();
    }
}
