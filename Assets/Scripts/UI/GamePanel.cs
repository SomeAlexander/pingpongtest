﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePanel : UIPanel
{
    public ScoreUI playerOneScoreUI;
    public ScoreUI playerTwoScoreUI;
}
