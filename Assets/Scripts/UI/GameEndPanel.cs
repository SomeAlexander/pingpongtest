﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEndPanel : UIPanel
{
    [SerializeField]
    private Text label;
    private const string TEXT_FORMAT = "{0} win!";

    public void Show(string winnerName) {
        base.Show();
        label.text = string.Format(TEXT_FORMAT, winnerName);
    }
}
