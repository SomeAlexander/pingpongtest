﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public IPlayer player;

    private void Update() {
        if(player != null) {
            transform.position = new Vector3(player.InputPosition, transform.position.y, transform.position.z);
        }
    }
}
