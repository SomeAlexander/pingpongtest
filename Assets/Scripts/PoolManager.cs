﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class PoolManager
{
    private static Dictionary<int, Stack<BaseBehaviour>> globalPool = new Dictionary<int, Stack<BaseBehaviour>>();

    public static T GetFromPool<T>(T prefab) where T : BaseBehaviour {
        T obj = null;
        int prefabId = prefab.GetInstanceID();
        Stack<BaseBehaviour> pool = null;
        if(globalPool.TryGetValue(prefabId, out pool) && pool != null && pool.Count > 0) {
            obj = (T)pool.Pop();
        }
        if(obj == null) {
            obj = MonoBehaviour.Instantiate<T>(prefab);
            obj.PoolId = prefab.GetInstanceID();
        }
        obj.gameObject.SetActive(true);
        return obj;
    }

    public static T GetFromPool<T>(T prefab, MonoBehaviour parent) where T : BaseBehaviour {
        T obj = GetFromPool(prefab, parent.transform);
        return obj;
    }

    public static T GetFromPool<T>(T prefab, Transform parent) where T : BaseBehaviour {
        T obj = GetFromPool(prefab);
        obj.transform.SetParent(parent);
        return obj;
    }

    public static void PushToPool<T>(T obj) where T : BaseBehaviour {
        obj.gameObject.SetActive(false);
        Stack<BaseBehaviour> pool = null;
        if(!globalPool.TryGetValue(obj.PoolId, out pool)) {
            pool = new Stack<BaseBehaviour>();
            globalPool.Add(obj.PoolId, pool);
        } else if(pool == null) {
            pool = new Stack<BaseBehaviour>();
        }
        pool.Push(obj);
    }
}
