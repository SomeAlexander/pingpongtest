﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Game : MonoBehaviour
{
    public PlayBoard playBoard;
    [SerializeField]
    private int scoreToWin;
    [SerializeField]
    private float roundDelay;
    public IPlayer playerOne;
    public IPlayer playerTwo;
    public event Action<IPlayer> onWin;
    private BaseBall ball;
    private bool gameIsActive;

    public void StartNewGame(IPlayer p1, IPlayer p2, BaseBall newBall) {
        gameIsActive = true;
        if(ball != null) {
            Destroy(ball.gameObject);
        }
        ball = newBall;
        playerOne = p1;
        playerTwo = p2;
        playBoard.Reset();
        playerOne.Score = 0;
        playerTwo.Score = 0;
        StartNewRound();
    }

    public void EndGame() {
        if(ball != null) {
            Destroy(ball.gameObject);
        }
        gameIsActive = false;
    }

    private void Update() {
        if(gameIsActive && playerOne != null && playerTwo != null)
            if(ball != null) {
                if(ball.transform.position.y > playBoard.Heigth / 2f) {
                    OnRoundEnd(winner: playerOne);
                } else if(ball.transform.position.y < -playBoard.Heigth / 2f) {
                    OnRoundEnd(winner: playerTwo);
                }
            } else {
                if(playerOne.Score >= scoreToWin) {
                    OnWin(playerOne);
                } else if(playerTwo.Score >= scoreToWin) {
                    OnWin(playerTwo);
                }
            }
    }

    private void OnRoundEnd(IPlayer winner) {
        winner.Score++;
        ball?.Reset();
        if(winner.Score >= scoreToWin) {
            OnWin(winner);
        } else {
            StartNewRound();
        }
    }

    public void StartNewRound() {
        if(ball != null) {
            ball.SetPosition(Vector3.zero);
        }
        StartCoroutine(RoundDelayRoutine());
    }

    private IEnumerator RoundDelayRoutine() {
        yield return new WaitForSeconds(roundDelay);
        float randY = UnityEngine.Random.value > 0.5f ? -playBoard.Heigth : playBoard.Heigth;
        float randX = UnityEngine.Random.Range(-playBoard.Width, playBoard.Width);
        ball?.StartMove(new Vector2(randX, randY));
    }

    private void OnWin(IPlayer winner) {
        ball?.SetPosition(Vector3.zero);
        EndGame();
        onWin?.Invoke(winner);
    }
}
