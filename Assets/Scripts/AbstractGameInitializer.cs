﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class AbstractGameInitializer : MonoBehaviour
{
    public abstract void Init(BallData ball, Action onSuccess);
    public abstract void Dispose();
}
