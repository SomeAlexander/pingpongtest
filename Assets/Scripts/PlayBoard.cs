﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayBoard : MonoBehaviour
{
    public Platform upperPlatform;
    public Platform bottomPlatform;
    [SerializeField]
    private float width;
    [SerializeField]
    private float heigth;
    [SerializeField]
    private PhysicsBlock leftSide;
    [SerializeField]
    private PhysicsBlock rightSide;

    public float Width { get { return width; } }
    public float Heigth { get { return heigth; } }

    public void Reset() {
        AlignPlatforms();
    }

    private void AlignPlatforms() {
        upperPlatform.transform.position = Vector3.up * heigth / 2f;
        bottomPlatform.transform.position = Vector3.down * heigth / 2f;
    }

#if UNITY_EDITOR
    private void OnValidate() {
        rightSide.transform.position = new Vector2(width / 2f, 0f);
        leftSide.transform.position = new Vector2(-width / 2f, 0f);
        rightSide.SetSize(0.2f, heigth);
        leftSide.SetSize(0.2f, heigth);
        AlignPlatforms();
    }
#endif
}
