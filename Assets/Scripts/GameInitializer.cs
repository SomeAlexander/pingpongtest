﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class GameInitializer : MonoBehaviour
{
    [SerializeField]
    private LocalGameInitializer localGame;
    [SerializeField]
    public NetworkGameInitializer networkGame;
    private AbstractGameInitializer currentGame;
    private BallData selectedBall;

    public void InitLocalGame(BallData ball, Action onSuccess) {
        currentGame = localGame;
        currentGame.Init(ball, onSuccess);
    }

    public void InitNetworkGame(BallData ball, Action onSuccess) {
        currentGame = networkGame;
        currentGame.Init(ball, onSuccess);
    }

    public void DisposeGame() {
        currentGame.Dispose();
    }
}
