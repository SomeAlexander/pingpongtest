﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ServiceProvider : MonoSingletone<ServiceProvider>
{
    [SerializeField]
    private Game m_game;
    [SerializeField]
    private Camera m_camera;
    [SerializeField]
    private GameInitializer m_gameInitializer;
    [SerializeField]
    private Balls m_balls;
    [SerializeField]
    private CustomNetworkManager m_networkManager;

    public Game Game { get { return m_game; } }
    public Camera Camera { get { return m_camera; } }
    public GameInitializer GameInitializer { get { return m_gameInitializer; } }
    public Balls Balls { get { return m_balls; } }
    public CustomNetworkManager NetworkManager { get { return m_networkManager; } }
}
