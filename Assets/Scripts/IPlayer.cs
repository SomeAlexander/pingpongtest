﻿using System;

public interface IPlayer
{
    event Action<int> onScoreChanged;
    int Score { get; set; }
    float InputPosition { get; }
    string Name { get; }
}
