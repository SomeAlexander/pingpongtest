﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour {
    [SerializeField]
    private Text scoreLabel;

    public void SetScore(int score) {
        scoreLabel.text = score.ToString();
    }
}
