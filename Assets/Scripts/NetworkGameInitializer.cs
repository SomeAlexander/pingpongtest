﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;

public class NetworkGameInitializer : AbstractGameInitializer
{
    public Action onPlayersReadyCallback;
    private BallData serverBall;

    public void OnEnable() {
        ServiceProvider.Instance.NetworkManager.onDisconnect += Dispose;
    }

    public void OnDisable() {
        ServiceProvider.Instance.NetworkManager.onDisconnect -= Dispose;
    }

    public override void Init(BallData ball, Action onSuccess) {
        serverBall = ball;
        onPlayersReadyCallback = onSuccess;
        if(serverBall == null) {//Clien
            ServiceProvider.Instance.NetworkManager.TryJoinMatch();
        } else {//Server
            ServiceProvider.Instance.NetworkManager.CreateMatch();
        }
        ServiceProvider.Instance.NetworkManager.onPlayersReady += InitNetworkGame;
    }

    public void InitNetworkGame() {
        BaseBall ball = null;
        //TODO: Refactor this
        bool isServer = serverBall != null;
        if(isServer) {
            ball = Instantiate(serverBall.prefab);
            NetworkServer.Spawn(ball.gameObject);
        }
        NetworkPlayer localPlayer = ServiceProvider.Instance.NetworkManager.LocalPlayer;
        NetworkPlayer remotePlayer = ServiceProvider.Instance.NetworkManager.RemotePlayer;
        //Set player position
        ServiceProvider.Instance.Game.playBoard.bottomPlatform.player = isServer ? localPlayer : remotePlayer;
        ServiceProvider.Instance.Game.playBoard.upperPlatform.player = isServer ? remotePlayer : localPlayer;
        ServiceProvider.Instance.Game.StartNewGame(localPlayer, remotePlayer, ball);
        //Set player view
        ServiceProvider.Instance.Camera.transform.localEulerAngles = isServer ? Vector3.zero : new Vector3(0f, 0f, 180f);
        onPlayersReadyCallback?.Invoke();
    }

    /*public void InitNetworkGameClient() {
        ServiceProvider.Instance.Game.playBoard.upperPlatform.player = ServiceProvider.Instance.NetworkManager.LocalPlayer;
        ServiceProvider.Instance.Game.playBoard.bottomPlatform.player = ServiceProvider.Instance.NetworkManager.RemotePlayer;
        ServiceProvider.Instance.Game.StartNewGame(ServiceProvider.Instance.NetworkManager.LocalPlayer,
                                                   ServiceProvider.Instance.NetworkManager.RemotePlayer,
                                                   null);
        //Set player view
        ServiceProvider.Instance.Camera.transform.localEulerAngles =;
        onPlayersReadyCallback?.Invoke();
    }*/

    public override void Dispose() {
        ServiceProvider.Instance.Game.EndGame();
        ServiceProvider.Instance.NetworkManager.StopMatch();
        ServiceProvider.Instance.NetworkManager.onPlayersReady -= InitNetworkGame;
    }
}
