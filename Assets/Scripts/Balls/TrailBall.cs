﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailBall : BaseBall
{
    [SerializeField]
    private TrailRenderer trail;

    public override void Reset() {
        base.Reset();
        trail?.Clear();
    }
}
