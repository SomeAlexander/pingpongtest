﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LocalPlayer : MonoBehaviour, IPlayer
{
    public PlayerInput inputs;
    public event Action<int> onScoreChanged;
    [SerializeField]
    private string m_name;
    private int m_score;

    public string Name { get { return m_name; } }

    public int Score {
        get {
            return m_score;
        }
        set {
            m_score = value;
            onScoreChanged?.Invoke(m_score);
        }
    }

    public float InputPosition {
        get {
            return inputs.HorizontalPosition;
        }
    }
}
