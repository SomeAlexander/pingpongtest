﻿using UnityEngine;

public abstract class BaseBehaviour : MonoBehaviour
{
    private int m_poolId;

    public int PoolId { get { return m_poolId; } set { m_poolId = value; } }

    protected virtual void Awake() { }
    protected virtual void OnEnable() { }
    protected virtual void OnDisable() { }
}
