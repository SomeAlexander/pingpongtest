﻿using UnityEngine;
using System.Collections;

public class MonoSingletone<T> : MonoBehaviour where T : MonoSingletone<T>
{
    private static T m_instance;
    public static T Instance {
        get {
            if(m_instance == null) {
                m_instance = FindObjectOfType<T>();
                if(m_instance == null) {
#if UNITY_EDITOR
                    Debug.LogWarningFormat("Singletone {0} are not exist on the scene!", typeof(T).ToString());
#endif
                }
            }
            return m_instance;
        }
        private set {
            m_instance = value;
        }
    }

    protected virtual void Awake() {
        if(Instance == null) {
            Instance = this as T;
        } else if(Instance != this) {
            Debug.LogWarningFormat("Singletone {0} alredy exist!", typeof(T).ToString());
        }
    }

    public static void CreateInstance() {
        if(Instance == null) {
            Instance = new GameObject(typeof(T).ToString()).AddComponent<T>();
        }
    }
}