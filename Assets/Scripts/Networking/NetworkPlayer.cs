﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class NetworkPlayer : NetworkBehaviour, IPlayer
{
    public PlayerInput inputs;
    public event Action<int> onScoreChanged;
    [SyncVar]
    private int m_score;
    private float m_inputPosition;
    private const string LOCAL_NAME = "You";
    private const string REMOTE_NAME = "Enemy";
    private const float INPUT_SEND_THRESHOLD = 0.005f;

    public string Name { get; set; }

    public int Score {
        get {
            return m_score;
        }
        set {
            if(isServer) {
                RpcSetScore(value);
            }
            m_score = value;
            onScoreChanged?.Invoke(value);
        }
    }

    public float InputPosition {
        get {
            return m_inputPosition;
        }
        set {
            if(Math.Abs(m_inputPosition - value) > INPUT_SEND_THRESHOLD) {
                CmdHorizontalPosition(value);
            }
            m_inputPosition = value;
        }
    }

    public void Start() {
        if(isLocalPlayer) {
            Name = LOCAL_NAME;
        } else {
            Name = REMOTE_NAME;
        }
        ServiceProvider.Instance.NetworkManager.AddPlayer(this);

    }

    [Command]
    private void CmdHorizontalPosition(float pos) {
        RpcHorizontalPosition(pos);
    }

    [ClientRpc]
    private void RpcHorizontalPosition(float pos) {
        if(!isLocalPlayer) {
            m_inputPosition = pos;
        }
    }

    [ClientRpc]
    private void RpcSetScore(int score) {
        Score = score;
    }

    private void Update() {
        if(isLocalPlayer) {
            InputPosition = inputs.HorizontalPosition;
        }
    }
}
