﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallSelectionButton : Button
{
    [SerializeField]
    private Text label;

    public void Init(BallData data) {
        label.text = data.name;
    }

    public override void OnPointerClick(UnityEngine.EventSystems.PointerEventData eventData) {
        base.OnPointerClick(eventData);
    }
}
