﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsBlock : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer sprite;
    [SerializeField]
    private new BoxCollider2D collider;

    public void SetSize(float width, float heigth) {
        Vector2 size = new Vector2(width, heigth);
        sprite.size = size;
        collider.size = size;
    }
}
