﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerInput : MonoBehaviour
{
    public float HorizontalPosition {
        get; private set;
    }

    private void Update() {
#if UNITY_EDITOR || UNITY_STANDALONE
        if(Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject()) {
            HorizontalPosition = ServiceProvider.Instance.Camera.ScreenToWorldPoint(Input.mousePosition).x;
        }
#else
        if(Input.touchCount > 0) {
            Touch touch = Input.GetTouch(0);
            if(!EventSystem.current.IsPointerOverGameObject()) {
                HorizontalPosition = ServiceProvider.Instance.Camera.ScreenToWorldPoint(touch.position).x;
            }
        }
#endif
    }
}
