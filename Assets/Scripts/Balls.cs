﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/Balls")]
public class Balls : ScriptableObject {
    public BallData[] balls;
	
}
